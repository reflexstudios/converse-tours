
import axios from 'axios';
import VueAxios from 'vue-axios';
import Start from './components/Start';
import Contact from './components/Contact';
import Thanks from './components/Thanks';
import Tour from './components/Tour';
import Loading from './components/Loading';
import Pay from './components/Pay';
import Datepicker from "vuejs-datepicker";

Vue.config.devtools = true;
Vue.use(VueAxios, axios);

window.book = new Vue({
  delimiters : ['{{!','}}'],
  el: '#book',
  data: {
    showBooking: true,
    bookingError: false,
    isLoading: true,
    isLoaded: false,
    csrfToken: '',
    siteUrl: '',
    tours: [],
    enquire: [],
    type: 'booking',
    step: 1,
    price: {
      adult: 0,
      childUnder5: 0,
      childAge8to13: 0,
      familyOf4: 0,
      private: 0,
    },
    selected: {
      tour_id: '',
      title: '',
      selectedDate: '',
      selectedTime: '',
      name: '',
      email: '',
      telephone: '',
      message: '',
      adults: 0,
      age8to13: 0,
      under5s: 0,
      family4: 0,
      privateTour: 'No',
      people: 0,
      cost: 0,
    }        
  },
  components: {
    Start,
    Contact,
    Thanks,
    Tour,
    Loading,
    Pay,
    Datepicker,
  },

  methods: {
    //
    setIsLoading: function(value) {
      this.isLoading = value;
    },

    sendEnquiry: function(selected) {
      this.bookingError = false;
      this.isLoading = true;
      this.type = 'enquiry';
      var formData = new FormData();
      formData.append('type_of_tour', selected.title);
      formData.append('name', selected.name);
      formData.append('email', selected.email);
      formData.append('phone', selected.telephone);
      formData.append('message', selected.message);
      this.axios.post(this.siteUrl + "/!/forms/enquiry", formData, {
        headers: {
          'X-CSRF-Token': this.csrfToken,
          'X-Requested-With': 'XMLHttpRequest',
        }
      })
      .then(response => {
          this.isLoading = false;
          const { success } = response.data;
          if(success == true) {
            this.step = 5;
          } else {
            this.bookingError = true;
          }
          //body[0].classList.remove('is-loading');
      }); 
      
    },
    submitBooking: function(stripeId) {
      this.bookingError = false;
      this.isLoading = true;
      this.type = 'booking';
      var formData = new FormData();
      var selected = this.selected;
      formData.append('tour_id', selected.tour_id);
      formData.append('tour', selected.title);
      formData.append('name', selected.name);
      formData.append('email', selected.email);
      formData.append('selectedDate', selected.selectedDate);
      formData.append('selectedTime', selected.selectedTime);
      formData.append('adults', selected.adults);
      formData.append('age8to13', selected.age8to13);
      formData.append('under5s', selected.under5s);
      formData.append('family4', selected.family4);
      formData.append('privateTour', selected.privateTour);
      formData.append('people', selected.people);
      formData.append('cost', selected.cost);
      formData.append('stripe_id', stripeId);

      this.axios.post(this.siteUrl + "/!/forms/booking", formData, {
        headers: {
          'X-CSRF-Token': this.csrfToken,
          'X-Requested-With': 'XMLHttpRequest',
        }
      })
      .then(response => {
        this.isLoading = false;
        const { success } = response.data;
        console.log("response: ", response.data);
        if(success == true) {
          console.log("success: ");
          this.step = 5;
        } else {
          console.log("fail: ");
          this.bookingError = true;
          this.step = 3;
        }
        //body[0].classList.remove('is-loading');
      }); 
    },
    setSelected: function(value) {
      this.selected = value;
      this.step = 3;
      //this.submitBooking();
    },
    selectTour: function(value) {
      console.log("found tour selceted");
      const regex = /([0-9]+([.][0-9]*)?|[.][0-9]+)/;
      this.selected.tour_id = value.id;
      this.selected.title = value.title;
      this.tours.forEach(row => {
        if(row.title === value.title) {
          let adult = 0;
          let childUnder5 = 'disable';
          let childAge8to13 = 0;          
          let familyOf4 = 0;
          let privatePrice = 0;
          let availableDates = null;

          if((row.adults_price) && (row.adults_price !== null)) {
            adult = row.adults_price.match(regex);
            adult = (adult.length > 0) ? adult[0] : 0;
          }

          if((row.childred_under_5_price) && (row.childred_under_5_price !== null)) {
            console.log('childred_under_5_price: ', row.childred_under_5_price);
            childUnder5 = row.childred_under_5_price.match(regex);
            childUnder5 = (childUnder5.length > 0) ? childUnder5[0] : 0;
            childUnder5 = parseFloat(childUnder5);
          }

          if((row.children_age_8_to_13_price) && (row.children_age_8_to_13_price !== null)) {
            childAge8to13 = row.children_age_8_to_13_price.match(regex);
            childAge8to13 = (childAge8to13.length > 0) ? childAge8to13[0] : 0;
          }

          if((row.family_of_4_price) && (row.family_of_4_price !== null)) {
            familyOf4 = row.family_of_4_price.match(regex);
            familyOf4 = (familyOf4.length > 0) ? familyOf4[0] : 0;
          }

          if((row.private_groups_price) && (row.private_groups_price !== null)) {
            privatePrice = row.private_groups_price.match(regex);
            privatePrice = (privatePrice.length > 0) ? privatePrice[0] : 0;
          }

          if((row.available_dates) && (row.available_dates !== null)) {
            availableDates = row.available_dates;
          }

          

          this.price = {
            adult: parseFloat(adult),

            childUnder5: childUnder5,
            childAge8to13: parseFloat(childAge8to13),
            familyOf4: parseFloat(familyOf4),
            private: parseFloat(privatePrice),
            available: availableDates,
          }
        }
      });

      this.step = 2;
    },
    enquireTour: function(value) {
      this.selected.title = value.title;
      this.step = 4;
      console.log("hello world");
    },
    backStep: function() {
      this.step = 1;
    },
    nextStep: function(value) {
      this.step = value;
    },
    backStart: function() {
      this.step = 1;
    },

    closeForm: function() {
      var body = document.getElementsByTagName('body');
      body[0].classList.remove('open-booking');
    },

    openBooking: function() {
      this.showBooking = true;
    },

    init: async function() {
      const parentElement = document.getElementById('book');
      const csrf_token = parentElement.getAttribute('data-csrf-token');
      const site_url = location.protocol + '//' + location.host;
      this.csrfToken = csrf_token;
      this.siteUrl = site_url;
      this.isLoading = true;

  
      if(this.tours.length === 0) {
        
        try {
            const res = await fetch(this.siteUrl + '/api/collections/tours/entries'); // Get the data from the API
            const { data } = await res.json() // Convert it to JSON
            const parent = this;
  
            data.forEach(row => {
              const include = row.include_pricing;
              if(include === true)  {
                const item = {
                  id: row.id,
                  url: row.url,
                  title: row.title,
                  time: row.time, 
                  image: row.featured_image.url,
                  adults_price: row.adults_price,
                  children_age_8_to_13_price: row.children_age_8_to_13_price,
                  childred_under_5_price: row.children_under_5_price,
                  family_of_4_price: row.family_of_4,
                  private_groups_price: row.private_groups_max_8_people,
                  available_dates: row.available_dates
                };
                
                parent.tours.push(item);
              } else {
                const item = {
                  id: row.id,
                  url: row.url,
                  title: row.title,
                };
                
                parent.enquire.push(item);
                console.log('enquire: ', this.enquire);
              }
            });
                    
          this.isLoading = false;
          } catch (e) {
            this.isLoading = false;
            // Handle your errors
            console.log('ERRORS: ', e);
          }
      }

     
      
    }
  },
  watch: {
    isLoaded: function (val) {
      if((val === true) && (this.tours.length === 0)) {
        this.init();
      }
    } 
  },
});
