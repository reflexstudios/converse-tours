<?php

namespace App\Listeners;

use App\Events\SubmissionSaved;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Statamic\Facades\Entry;
use Statamic\Facades\Collection;
use Carbon\Carbon;

class BookingSaved
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        //
        $stripe_id = (($event->form->stripe_id !== null) && ($event->form->stripe_id !== "")) ? $event->form->stripe_id : "";
        if($stripe_id !== '') {
            $handle = $event->form->tour_id;
            $number_of_people = (int)$event->form->people;
            $entry = \Statamic\Facades\Entry::find($handle);
            $selectedDate = date('Y-m-d', strtotime($event->form->selectedDate));
            $selectedTime = $event->form->selectedTime;
            $dates = $entry->available_dates;

    
            if($dates !== null) {
                foreach($dates as $key=>$row) {
                    if(($selectedDate === $row['date']) && ($selectedTime === $row['time'])) {
                        $dates[$key]['number_of_places'] = $row['number_of_places'] - $number_of_people;
                    }
                }


                $entry->set('available_dates', $dates);
                $entry->save();
            }

        }

    }
}
