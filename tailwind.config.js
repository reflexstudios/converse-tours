module.exports = {
    mode: 'jit',
    purge: [
      './resources/**/*.antlers.html',
      './resources/**/*.js',
      './resources/**/*.vue',
    ],
    darkMode: false, // or 'media' or 'class'
    theme: {
      extend: {
  
        screens: {
          '3xl': '1600px',
        },
        
        fontSize: {
            'xxs': '0.625rem',      //  10px
            '4xl': '2.5rem',      //  40px
            '8xl': '5rem',      //  80px
            '10xl': '7rem',     //  112px
        },
  
      
        
      
        lineHeight: {    
          '3.5' : '0.875rem',      // 14 - used on btns 
          '5.5' : '1.375rem',      // 22 - used on titles    
          '7.5' : '1.875rem',      // 30 - used on titles 
  
        },
  
        letterSpacing: {  
          
          'tight': '-0.015625rem',      // -0.25px    
          'x-tight': '-0.028125rem',    // -0.45px
          'xx-tight': '-0.0375rem',     // -0.6px
          'xxx-tight': '-0.046875rem',  // -0.75px;
          
        
         },
    
        colors: {
          primary:'#151E48',            // Blue
          secondary:'#FF612A',          // Orange
          orange:'#FF832A',             // Orange on hovers
          paleblue:'#202D6C',           // Blue on hovers
          paleorange:'#FFE0D1',         // Pale Orange
          palegrey:'#ECEDF0',           // Pale Gray
          },        
       
          spacing: {
            '1.25': '0.3125rem',      //5px
            '3.5': '0.875rem',        //14px 
            '2.5': '0.625rem',        //10px 
            '7.5': '1.875rem',        //30px
            '6.625': '1.65625rem',    //26.5px       
            '12.5': '3.125rem',       //50px
            '15': '3.75rem',          //60px 
            '17.5': '4.375rem',       //70px 
            '22.5': '5.625rem',       //90px 
            '29.5': '7.375rem',       //118px
            '25': '6.25rem',          //100px  
            '30': '7.5rem',           //120px
            '31.5': '7.875rem',       //126px
            '38': '9.5rem',           //152px
            '39.5': '9.875rem',       //158px
            '41.5': '10.375rem',      //166px
            '50': '12.5rem',          //200px 
            '72.5': '18.125rem',      //290px 
            '82': '20.25rem',         //328px 
            '86.75': '21.6875rem',    //347px 
            '99.5': '24.875rem',      //398px 
            '110': '27.5rem',         //440px 
            '160': '40rem',           //640px
            '140': '35rem',           //560px 
            '146.5': '36.625rem',     //586px 
            '175.5': '43.875rem',     //702px
            '200': '50rem',           //800px  
            '220.5': '55.125rem',     //882px 
            '287.25': '69.5625rem',   //1113px
           
            
          },
       
          maxWidth: {
              
           }
        },
  
        
        
       
      fontFamily: {   
        
        'Poppins': ['Poppins', 'sans-serif'],
       
      },      
     
    },
    variants: {
      extend: {
        scale: ['active', 'group-hover'],
        textColor: ['group-hover'],
        borderRadius: ['hover', 'focus'],
        mixBlendMode: ['hover', 'focus'],
        
        opacity: ['hover', 'group-hover'],
        rotate: ['group-hover', 'focus-within', 'focus'],
        transform: ['group-hover', 'focus-within', 'focus'],
        translate: ['group-hover', 'focus-within', 'focus'],
        transitionDuration: ['group-hover'],
        transitionProperty: ['group-hover'],
        transitionTimingFunction: ['group-hover'],
        margin: ['group-hover'],
        fill: ['group-hover'],
        display: ['group-hover'],
        height: ['responsive', 'hover', 'focus']
      },
    },
    plugins: [],
  }