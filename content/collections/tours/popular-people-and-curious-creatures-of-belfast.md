---
id: a7a127ab-5bcd-4cca-b0ca-382cc3b894d5
blueprint: tours
title: 'Popular People & Curious Creatures of Belfasts'
make_featured_tour: true
title_subtext: 'This tour celebrates the people of Belfast past & present and takes a light- hearted look at the creatures who lived alongside them and still look down upon them.'
body_large_text: 'What shapes a city? we all know it is about more than bricks & mortar, it’s about hearts and minds'
body_subtext:
  -
    type: paragraph
    content:
      -
        type: text
        text: 'What shapes a city? – we all know it is about more than bricks & mortar, it’s about hearts and minds.'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'This tour celebrates the people of Belfast past & present and takes a light- hearted look at the creatures who lived alongside them and still look down upon them.'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Discover rebels and forgotten heroes as well as our current movers and shakers from Michelin star chefs to award winning actors.'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'We start at the yellow Jaffe Fountain, beside Bittles pub at Victoria Square and take a circular route up High Street and along the Lagan River finishing at City Hall, just minutes from our starting point.'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'We’ll imagine life down the narrow Entries, stroll down High Street, skip down Skipper Street and take in the sights and sounds of the River Lagan, before appreciating the beauty of the Courts and City Hall. On our way we’ll encounter elephants, dragons and even unicorns.'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'With a dash of humour and a good sprinkling of history this tour has something for everyone.'
meeting_point_name: 'The yellow Jaffe Fountain, beside Bittles pub at Victoria Square'
include_time: true
time: '2 Hours'
include_miles: true
miles: '1.5'
is_family_friendly: true
is_pram_and_wheelchair_friendly: true
is_adults_only: false
include_pricing: true
updated_by: 1ebc2068-9f47-4791-9acf-c2eec6952256
updated_at: 1655980646
featured_image: placeholders/gallery.jpg
what_3_words_1: intelligible
what_3_words_2: kept
what_3_words_3: slave
include_meeting_point: true
meeting_point_picture: placeholders/cons1.jpg
is_tour: Tour
include_covid_banner: true
available_dates:
  -
    date: '2022-07-10'
    time: '10:00'
    number_of_places: 10
  -
    date: '2022-07-11'
    time: '10:00'
    number_of_places: 21
  -
    date: '2022-07-16'
    time: '10:00'
    number_of_places: 22
  -
    date: '2022-07-16'
    time: '14:00'
    number_of_places: '25'
accordion:
  -
    title: 'Popular People'
    text:
      -
        type: bullet_list
        content:
          -
            type: list_item
            content:
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'Sir Otto Jaffe'
          -
            type: list_item
            content:
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'Pubs, Film Oscar winners, actors'
          -
            type: list_item
            content:
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'Young Entrepreneurs pubs'
          -
            type: list_item
            content:
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'Henry Joy McCracken – United Irishmen'
          -
            type: list_item
            content:
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'Vikings, Normans, castles'
          -
            type: list_item
            content:
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'Jonathan Swift'
          -
            type: list_item
            content:
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'Skateboarders, bands, LOD'
          -
            type: list_item
            content:
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'Artists from Lavery to Street Art, John Luke, Colin Davidson etc'
          -
            type: list_item
            content:
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'Prince Albert, Queen Vic - Victorians'
          -
            type: list_item
            content:
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'WW2 Soldiers'
          -
            type: list_item
            content:
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'Harland & Wolff, Shorts, Lilian Bland'
          -
            type: list_item
            content:
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: Emigrants
          -
            type: list_item
            content:
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'Award winning Michelin Star chefs'
          -
            type: list_item
            content:
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: "Harmony\_"
          -
            type: list_item
            content:
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'Belfast City Council'
          -
            type: list_item
            content:
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'Civil Rights Demonstrators'
          -
            type: list_item
            content:
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'Protesters & Revellers – City Hall'
          -
            type: list_item
            content:
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'Musicians from Punk to Flautists'
          -
            type: list_item
            content:
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'Farming, markets'
          -
            type: list_item
            content:
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'Brief Troubles, terrorists, criminals Burke & O’Hare (not very popular!)'
          -
            type: list_item
            content:
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'William Pirrie'
          -
            type: list_item
            content:
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'Seaman Magennis'
    type: new_accordion_item
    enabled: true
  -
    title: 'Curious Creatures'
    text:
      -
        type: bullet_list
        content:
          -
            type: list_item
            content:
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: Dragons
          -
            type: list_item
            content:
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: Unicorns
          -
            type: list_item
            content:
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'Seals,'
          -
            type: list_item
            content:
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: "\_Big Fish,\_"
          -
            type: list_item
            content:
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: "Bird of Prey,\_"
          -
            type: list_item
            content:
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: "Hounds,\_"
          -
            type: list_item
            content:
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: "Elephants,\_"
          -
            type: list_item
            content:
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'Mermaids,'
          -
            type: list_item
            content:
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'Owls & Otters'
          -
            type: list_item
            content:
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: "Phoenix\_"
          -
            type: list_item
            content:
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: "Sphinx\_"
          -
            type: list_item
            content:
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: Dolphins
          -
            type: list_item
            content:
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: Seahorse
          -
            type: list_item
            content:
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: "\_Sheep on the Road"
    type: new_accordion_item
    enabled: true
  -
    title: 'Places & Photostops'
    text:
      -
        type: bullet_list
        content:
          -
            type: list_item
            content:
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'Starts Jaffe Fountain Vic Square'
          -
            type: list_item
            content:
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'Big Fish'
          -
            type: list_item
            content:
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'Customs House'
          -
            type: list_item
            content:
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'Scottish Provident'
          -
            type: list_item
            content:
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'The Merchant Hotel'
          -
            type: list_item
            content:
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'Albert Clock'
          -
            type: list_item
            content:
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'Corn Market'
          -
            type: list_item
            content:
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'The Royal Courts of Justice'
          -
            type: list_item
            content:
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'St Georges Market'
          -
            type: list_item
            content:
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'The Entries'
          -
            type: list_item
            content:
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'High Street'
          -
            type: list_item
            content:
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'Harmony, Queens Bridge'
    type: new_accordion_item
    enabled: true
adults_price: '2'
children_age_8_to_13_price: '5'
family_of_4: '40'
private_groups_max_8_people: '100'
children_under_5_price: '0'
---
