---
id: 3f186d62-8ce2-45bd-a430-4849fbb501dd
blueprint: tours
is_tour: Tour
title: 'Private Bookings & Bespoke Tours'
title_subtext: "Local, professional, well informed, entertaining, friendly, well organised, good time manager, calm\_"
body_large_text: 'Experienced Local guide, 10 years guiding experience'
body_subtext:
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: textAlign
            attrs:
              align: start
        text: 'Experienced Local guide, 10 years guiding experience'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: textAlign
            attrs:
              align: start
        text: 'Blue Badge Qualified & Insured'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: textAlign
            attrs:
              align: start
        text: 'Member of Northern Ireland Tour Guiding Association'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: textAlign
            attrs:
              align: start
        text: "Local, professional, well informed, entertaining, friendly, well organised, good time manager, calm\_"
make_featured_tour: false
include_covid_banner: false
is_family_friendly: false
is_pram_and_wheelchair_friendly: false
is_adults_only: false
include_pricing: false
include_meeting_point: false
updated_by: 1ebc2068-9f47-4791-9acf-c2eec6952256
updated_at: 1648229189
---
