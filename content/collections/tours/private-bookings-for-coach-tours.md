---
id: 9c6b1d2c-c7f8-4a94-b164-c7a08793d768
blueprint: tours
is_tour: Tour
title: 'Private Bookings For Coach Tours'
title_subtext: 'Local, professional, well informed, entertaining, friendly, well organised, good time manager, calm'
other_images:
  - placeholders/tour1.jpg
body_large_text: 'Experienced Local guide, 10 years guiding experience'
body_subtext:
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Experienced Local guide, 10 years guiding experience'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Blue Badge Qualified & Insured'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Member of Northern Ireland Tour Guiding Association'
  -
    type: paragraph
    content:
      -
        type: text
        text: "Local, professional, well informed, entertaining, friendly, well organised, good time manager, calm\_"
make_featured_tour: false
include_covid_banner: false
include_time: false
include_miles: false
is_family_friendly: false
is_pram_and_wheelchair_friendly: false
is_adults_only: false
include_pricing: false
include_meeting_point: false
updated_by: 1ebc2068-9f47-4791-9acf-c2eec6952256
updated_at: 1648238838
---
