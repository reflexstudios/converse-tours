---
id: 699dc73e-30c7-4c46-87c7-052805107d8b
blueprint: tours
title: 'Top Ten Belfast Express Walking Tour'
make_featured_tour: false
title_subtext: "For Converse Tours, these streets are home and we’d love to show you around.\_It’s quick and easy. Nice and breezy.\_ Just show me the good stuff tour.\_"
body_large_text: 'A great tour for all the family'
body_subtext:
  -
    type: paragraph
    content:
      -
        type: text
        text: "Ever arrive in a new city and feel a bit lost – this tour will help you settle in straight away. For Converse Tours, these streets are home and we’d love to show you around.\_It’s quick and easy. Nice and breezy.\_ Just show me the good stuff tour.\_"
  -
    type: paragraph
    content:
      -
        type: text
        text: "As well as the Top Ten Sights we’ll guide you to the best pubs, shops & interesting stops. Get the best pictures and find out what Belfast’s really about.\_"
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Not just another bus tour'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Better pictures than from a moving bus'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Feel like you’ve done something out in the air'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Discover something new, ask questions, join in the conversation'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: bold
        text: 'This tour is fast-paced so it’s best to wear comfortable shoes & layers'
include_time: true
time: '3 Hours'
include_miles: true
miles: '4 Miles'
is_family_friendly: false
is_pram_and_wheelchair_friendly: false
is_adults_only: true
include_pricing: true
updated_by: 1ebc2068-9f47-4791-9acf-c2eec6952256
updated_at: 1648502993
featured_image: 20210529_110207.jpg
other_images:
  - 20210428_122927.jpg
adults_price: '20'
children_age_8-13_price: £16
meeting_point_name: 'St Annes Square'
what_3_words_1: hammer
what_3_words_2: lamp
what_3_words_3: units
meeting_point_picture: placeholders/cons4.jpg
include_meeting_point: true
is_tour: Tour
include_covid_banner: true
tour_dates:
  start: '2021-09-04'
  end: '2021-09-08'
available_dates:
  -
    date: '2022-04-05'
    time: '10:00'
    number_of_places: 21
  -
    date: '2022-04-05'
    time: '12:00'
    number_of_places: '25'
  -
    date: '2022-04-06'
    time: '10:00'
    number_of_places: 6
  -
    date: '2022-04-06'
    time: '12:00'
    number_of_places: '25'
  -
    date: '2022-05-06'
    time: '12:00'
    number_of_places: '25'
  -
    date: '2022-05-07'
    time: '12:00'
    number_of_places: '25'
  -
    date: '2022-05-09'
    time: '12:00'
    number_of_places: '25'
---
