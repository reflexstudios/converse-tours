---
id: 7709b749-571d-4feb-ae77-ca1347cb93e5
blueprint: tours
title: 'Titanic Tales & River Walk Tour of belfast'
make_featured_tour: false
title_subtext: |-
  This is suitable for those who enjoy fresh air and a good invigorating walk along the Lagan river.
  Stopping to share with you stories of the vision, tragedy and mystery of Titanic.
body_large_text: 'A lovely blend of exercising both your body and mind'
body_subtext:
  -
    type: paragraph
    content:
      -
        type: text
        text: 'This is suitable for those who enjoy the fresh air and a good invigorating walk along the Lagan river. Stopping to share with you stories of the vision, tragedy and mystery of Titanic. A lovely blend of exercising both your body and mind. (Unfortunately, I can’t take you up the Lagan in a bubble)'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Advisable bring a rain/wind proof jacket & umbrella'
include_time: true
time: '2 Hours 30 Mins'
include_miles: true
miles: 3Miles
is_family_friendly: true
is_pram_and_wheelchair_friendly: false
is_adults_only: true
include_pricing: true
updated_by: 1ebc2068-9f47-4791-9acf-c2eec6952256
updated_at: 1648507186
featured_image: placeholders/tour1.jpg
is_tour: Tour
include_meeting_point: false
include_covid_banner: true
adults_price: '15'
available_dates:
  -
    date: '2022-04-12'
    time: '11:00'
    number_of_places: '25'
  -
    date: '2022-04-12'
    time: '15:00'
    number_of_places: '25'
  -
    date: '2022-04-19'
    time: '10:00'
    number_of_places: '25'
---
