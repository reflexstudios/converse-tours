---
id: c59514f2-6e0f-40f7-8ae8-fa73ab590a3d
blueprint: pages
title: Reviews
author: 4370140d-9148-40ed-b722-34c4fe76874d
updated_by: 1ebc2068-9f47-4791-9acf-c2eec6952256
updated_at: 1648462983
general_content:
  -
    type: set
    attrs:
      values:
        type: small_banner
        title: Reviews
        subtext: 'People are at the heart of my tours and visitors thank me for taking good care of them, being friendly, entertaining and knowledgeable. Read my customer comments below.'
  -
    type: set
    attrs:
      values:
        type: reviews
  -
    type: set
    attrs:
      values:
        type: button
        button_text: 'View Tours'
        button_link: 'entry::fd80e273-c15b-4531-87b2-fb96b36afad8'
  -
    type: paragraph
  -
    type: set
    attrs:
      values:
        type: companies_worked_with
meta_title: 'Belfast Walking Tour Reviews | Converse Tours'
---
