---
id: home
blueprint: home
title: Home
template: home
general_content:
  -
    type: paragraph
  -
    type: set
    attrs:
      values:
        type: hero_banner
        include_testimonials: false
        main_title: 'Belfast walking tours'
        subtext: |-
          My tours aim to tell you a story and entertain you. Together we will discover the who’s who and the
          what’s what of the city.
        hero_banner_image: hero3.jpg
        bottom_logo: good-to-go.png
        include_reviews: true
        include__featured_reviews: true
        include_featured_reviews: true
  -
    type: set
    attrs:
      values:
        type: full_width_cta
        skills:
          -
            title: MULTILINGUAL
            subtext: 'English, French and German'
            type: new_skill
            enabled: true
            image: placeholders/logos/multilingual.svg
          -
            title: 'FRIENDLY & ENTERTAINING'
            subtext: 'English, French and German'
            type: new_skill
            enabled: true
            image: placeholders/logos/face.svg
          -
            title: '10 YEARS EXPERIENCE'
            subtext: 'English, French and German'
            type: new_skill
            enabled: true
            image: placeholders/logos/experience.svg
          -
            title: 'BLUE BADGE QUALIFIED'
            subtext: 'English, French and German'
            type: new_skill
            enabled: true
            image: placeholders/logos/bluebadge.svg
  -
    type: set
    attrs:
      values:
        type: 2_col_cta
        cta_button:
          -
            title: 'Private Bookings For Coach Tours'
            subtext: 'Find out about hop on hop off coach tours'
            link_to_tour: 'entry::699dc73e-30c7-4c46-87c7-052805107d8b'
            type: new_button
            enabled: true
          -
            title: 'Ghost Tours in Belfast at Night'
            subtext: 'Click here to find about spooky ghost tours'
            link_to_tour: 'entry::7709b749-571d-4feb-ae77-ca1347cb93e5'
            type: new_button
            enabled: true
  -
    type: set
    attrs:
      values:
        type: text_with_images
        title: 'Belfast, theres no place quite like it'
        text:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'I would love to take you on a tour around my hometown Belfast. There is no place quite like it; a city with a strong spirit and a warm heart and people with plenty to say.'
              -
                type: hard_break
              -
                type: hard_break
              -
                type: text
                text: 'People are at the heart of Belfast - I look forward to sharing their stories and a few of my own with you as we walk the streets together. We will discover the who’s who and the what’s what of the city.'
              -
                type: hard_break
              -
                type: hard_break
              -
                type: text
                text: 'Whether you are an independent traveller, a small group, family or with colleagues Converse tours offers you an easy introduction and a warm welcome to Belfast.'
        button_text: 'More about Converse Tours'
        button_link: 'entry::c71f2d88-c3ec-431e-985f-ca47f6fe2613'
        image_1: placeholders/cons.jpg
        image_2: placeholders/cons3.jpg
        image_3: placeholders/cons2.jpg
        image_4: placeholders/cons1.jpg
        image_5: placeholders/tour1.jpg
        flip_text_and_images: false
updated_by: 1ebc2068-9f47-4791-9acf-c2eec6952256
updated_at: 1654701346
hero_banner_image: hero3.jpg
main_title: 'Belfast walking tours'
subtext: |-
  My tours aim to tell you a story and entertain you. Together we will discover the who’s who and the
  what’s what of the city.
bottom_logo: good-to-go.png
include_featured_reviews: true
flip_text_and_images: false
skills:
  -
    image: placeholders/logos/multilingual.svg
    title: MULTILINGUAL
    subtext: 'English, French and German'
    type: new_skill
    enabled: true
  -
    image: placeholders/logos/face.svg
    title: 'FRIENDLY & ENTERTAINING'
    subtext: 'Tours that spark conversation'
    type: new_skill
    enabled: true
  -
    title: '10 YEARS EXPERIENCE'
    type: new_skill
    enabled: true
    image: placeholders/logos/experience.svg
    subtext: 'Internationally and Locally'
  -
    image: placeholders/logos/bluebadge.svg
    title: 'BLUE BADGE QUALIFIED'
    subtext: 'Highest qualification in guiding'
    type: new_skill
    enabled: true
select_featured_tours:
  - a7a127ab-5bcd-4cca-b0ca-382cc3b894d5
  - 7709b749-571d-4feb-ae77-ca1347cb93e5
  - 699dc73e-30c7-4c46-87c7-052805107d8b
cta_button:
  -
    title: 'Private Bookings For Coach Tours'
    subtext: 'Find out about hop on hop off coach tours'
    link_to_tour: 'entry::9c6b1d2c-c7f8-4a94-b164-c7a08793d768'
    type: new_button
    enabled: true
  -
    title: 'Private Bookings For Coach Tours'
    subtext: 'Find out about private & bespoke tour offerings'
    link_to_tour: 'entry::3f186d62-8ce2-45bd-a430-4849fbb501dd'
    type: new_button
    enabled: true
text:
  -
    type: paragraph
    content:
      -
        type: text
        text: 'I would love to take you on a tour around my hometown Belfast. There is no place quite like it; a city with a strong spirit and a warm heart and people with plenty to say.'
      -
        type: hard_break
      -
        type: hard_break
      -
        type: text
        text: 'People are at the heart of Belfast - I look forward to sharing their stories and a few of my own with you as we walk the streets together. We will discover the who’s who and the what’s what of the city.'
      -
        type: hard_break
      -
        type: hard_break
      -
        type: text
        text: 'Whether you are an independent traveller, a small group, family or with colleagues Converse tours offers you an easy introduction and a warm welcome to Belfast.'
title_text: 'Belfast, there’s no place quite like it'
button_text: 'more about converse tours'
button_link: 'entry::c71f2d88-c3ec-431e-985f-ca47f6fe2613'
image_1: placeholders/cons.jpg
image_2: placeholders/cons3.jpg
image_3: placeholders/cons2.jpg
image_4: placeholders/cons1.jpg
image_5: placeholders/cons4.jpg
meta_title: 'Converse Tours | Walking Tours Belfast'
meta_description: |-
  Let Converse Tours tell you a story and entertain you. Together we will discover the who’s who and the
  what’s what of the city.
---
Welcome to your new Statamic website.
