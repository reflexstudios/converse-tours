---
id: c59d7491-574e-4592-b116-947ba84fcb14
blueprint: pages
title: 'Covid-19 Safety Measures'
general_content:
  -
    type: set
    attrs:
      values:
        type: small_banner
        title: 'Covid-19 Safety Measures'
        subtext: 'We have taken all of the steps possible to ensure your saftety on our tours'
  -
    type: bullet_list
    content:
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Guide will wear mask in enclosed spaces'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Guide will carry hand sanitiser for customer use'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Guide is vaccinated'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Social distancing will be observed'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Track & Trace'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Toilets will be within easy reach for hand washing'
      -
        type: list_item
        content:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'We have been awarded with the “We’re good to go” badge in partnership with Tourism NI'
  -
    type: paragraph
    content:
      -
        type: image
        attrs:
          src: 'asset::assets::YXNzZXRzL3BsYWNlaG9sZGVycy9sb2dvcy9nb29kLXRvLWdvLWNvbG9yLnBuZw==.png'
          alt: null
updated_by: 4370140d-9148-40ed-b722-34c4fe76874d
updated_at: 1630070955
---
