---
id: fd80e273-c15b-4531-87b2-fb96b36afad8
blueprint: pages
title: Tours
author: 4370140d-9148-40ed-b722-34c4fe76874d
updated_by: 2ae4fa4f-129a-4456-9071-5373766ef6ab
updated_at: 1648517042
general_content:
  -
    type: set
    attrs:
      values:
        type: small_banner
        title: Tours
        subtext: 'People are at the heart of my tours and visitors thank me for taking good care of them, being friendly, entertaining and knowledgeable.'
  -
    type: set
    attrs:
      values:
        type: featured_tours
        select_featured_tours:
          - a7a127ab-5bcd-4cca-b0ca-382cc3b894d5
          - 699dc73e-30c7-4c46-87c7-052805107d8b
          - 7709b749-571d-4feb-ae77-ca1347cb93e5
  -
    type: set
    attrs:
      values:
        type: 2_col_cta
        cta_button:
          -
            title: 'private bookings for coach tours'
            subtext: 'Find out about hop on hop off coach tours'
            type: new_button
            enabled: true
            link_to_tour: 'entry::9c6b1d2c-c7f8-4a94-b164-c7a08793d768'
          -
            title: 'Private Bookings & Bespoke Tours'
            subtext: 'Find out about private & bespoke tour offerings'
            link_to_tour: 'entry::3f186d62-8ce2-45bd-a430-4849fbb501dd'
            type: new_button
            enabled: true
meta_title: 'Walking Tours Belfast | Converse Tours'
---
