---
id: c71f2d88-c3ec-431e-985f-ca47f6fe2613
blueprint: pages
title: About
author: 4370140d-9148-40ed-b722-34c4fe76874d
updated_by: 1ebc2068-9f47-4791-9acf-c2eec6952256
updated_at: 1648462935
general_content:
  -
    type: set
    attrs:
      values:
        type: small_banner
        title: About
        subtext: 'People are at the heart of my tours and visitors thank me for taking good care of them, being friendly, entertaining and knowledgeable.'
  -
    type: set
    attrs:
      values:
        type: banner_image
        banner_image: placeholders/hero.jpg
        image_position: Top
        background_position: '30'
  -
    type: heading
    attrs:
      level: 2
    content:
      -
        type: hard_break
      -
        type: text
        marks:
          -
            type: textAlign
            attrs:
              align: center
        text: 'I look forward to walking and talking with you'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'I was born in Belfast and have grown with the city, watching it turn both challenging and exciting corners to become the vibrant and diverse city that it is today.'
      -
        type: hard_break
      -
        type: hard_break
      -
        type: text
        text: 'I started my business in 2013 and the name Converse comes from an appreciation of good shoes, good conversation and from my love of looking at things conversely (from another angle).'
      -
        type: hard_break
      -
        type: hard_break
      -
        type: text
        text: 'People are at the heart of my tours and visitors thank me for taking good care of them, being friendly, entertaining and knowledgeable - “We can tell you love your job and your country”.'
      -
        type: hard_break
      -
        type: hard_break
      -
        type: text
        text: 'I have invested considerable time in my qualifications, BA Dual Hons, PGCE, Dip Marketing, Blue Badge Guide in English French & German, ILM Level 3 & several years at the school of life to be able to offer you a professional and well organised tour.'
      -
        type: hard_break
      -
        type: hard_break
      -
        type: text
        text: 'I look forward to walking and talking with you.'
  -
    type: set
    attrs:
      values:
        type: signature
  -
    type: paragraph
  -
    type: set
    attrs:
      values:
        type: button
        button_text: 'View Tours'
        button_link: 'entry::fd80e273-c15b-4531-87b2-fb96b36afad8'
  -
    type: paragraph
    content:
      -
        type: hard_break
  -
    type: set
    attrs:
      values:
        type: companies_worked_with
meta_title: 'About Converse Tours | Belfast Walking Tours'
---
