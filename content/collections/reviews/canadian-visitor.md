---
id: c75c0901-c05a-4735-b9dc-00ec73adea6e
blueprint: reviews
type_of_tour:
  - Walking
title: 'Canadian Visitor'
country_flag: placeholders/flags/canada.svg
review: 'We have no problem at all understanding you, a lovely clear accent'
updated_by: 4370140d-9148-40ed-b722-34c4fe76874d
updated_at: 1629969040
---
