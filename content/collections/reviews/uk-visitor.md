---
id: 4ebf67b1-8c2f-446d-b2db-5d671d2e05ab
blueprint: reviews
type_of_tour:
  - Walking
  - Featured
title: 'UK Visitor'
country_flag: placeholders/flags/uk.svg
review: 'Delightful way to spend an afternoon, very informative'
updated_by: 4370140d-9148-40ed-b722-34c4fe76874d
updated_at: 1629970841
---
