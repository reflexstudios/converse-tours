---
id: 9ebbf71c-1dc1-4487-9e65-03a6c86e4222
blueprint: reviews
type_of_tour:
  - Coach
title: 'US Visitor'
country_flag: placeholders/flags/usa.svg
review: 'I loved the food stops along the way.'
updated_by: 4370140d-9148-40ed-b722-34c4fe76874d
updated_at: 1629969111
---
