---
id: 64d6021e-a81e-46a0-a10b-39eb6761c339
blueprint: reviews
type_of_tour:
  - Walking
  - Featured
title: 'US Kentucky'
country_flag: placeholders/flags/usa.svg
review: 'Very informative & very entertaining.'
updated_by: 4370140d-9148-40ed-b722-34c4fe76874d
updated_at: 1629970850
---
