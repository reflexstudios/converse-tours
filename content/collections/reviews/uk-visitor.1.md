---
id: 46708bed-f614-4258-84bf-65b089d96c79
blueprint: reviews
type_of_tour:
  - Walking
title: 'UK Visitor'
country_flag: placeholders/flags/uk.svg
review: 'Everyone is saying what a lovely personality you have – just right for the job'
updated_by: 4370140d-9148-40ed-b722-34c4fe76874d
updated_at: 1629969023
---
