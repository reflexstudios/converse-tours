---
id: 922780b1-a363-43da-813c-5b14a40ff536
blueprint: reviews
type_of_tour:
  - Walking
  - Coach
  - Featured
title: 'Irish Visitor'
country_flag: placeholders/flags/ireland.svg
review: 'You tell a class story!'
updated_by: 4370140d-9148-40ed-b722-34c4fe76874d
updated_at: 1629971202
---
