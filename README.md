## Converse Tours
        
        

## Up and Running

• PHP - >v7.3            
• Node - v16  
• Valet - valet link  
• Browser Sync - npm run watch



## Locations

• CSS - tailwind.css            
• JS - site.js            



## Included

• Statamic Pro - ✓            
• Mailtrap - ✓    



## Tailwind Extends
              
• rotate: ['group-hover', 'focus-within', 'focus'],                        
•  transform: ['group-hover', 'focus-within', 'focus'],                        
•  translate: ['group-hover', 'focus-within', 'focus'],                         
•  transitionDuration: ['group-hover'],                         
•  transitionProperty: ['group-hover'],                         
•  transitionTimingFunction: ['group-hover'],                         
•  margin: ['group-hover'],                         
•  fill: ['group-hover'],                         
•  display: ['group-hover'],                         
•  height: ['responsive', 'hover', 'focus']                     
                

                        
## User Login Details
                
• Email -  hello@reflex-studios.com                    
• Super User -  ✓                    
• Password - π
• Wee jokes, it's 123456789


## Dev Site 
converse.reflex-dev.com
username: conversereflexde
password: GE8?epa6gzHz
ssh -p 4835 conversereflexde@185.211.22.175

## StripeConverse Test Acc
Publishable key: 
pk_test_51KChqNIQOLlaayYOMZnl9zIvKnkvlzAu0tPYOYFdpW17HVgMZveuCUQVB2MB4j8UjQAJuIoppCdkEh54vtmlQIe400o8C856Yk

Secret key: 
sk_test_51KChqNIQOLlaayYOoPa7mgdln4driaLKpktmen2SQDHKP6dpcfibK9joxlNMemESfg07S5JbbMGGEDurVQyoR6Uf00CtBml46b

## StripeConverse Live Acc
Publishable key: 
pk_live_51KChqNIQOLlaayYOLCVsYr9sYy8qtcIXGHifFzEHZT0gaqoH6IdorGbmX9uMXnFlYIZ2vdXVkUTn7iXxxzkPhb1400Re3GLkF9
Secret key:
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